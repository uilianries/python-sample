# Python Hello Sample

Python package that print hello.

### install

To apply this template in your current environment:

    $ conan config install http://gitlab.khomp.corp/uilian/conan-config.git

### Local customization

E.g If you wish to set libcxx on profile linux-gcc54-amd64:

    $ conan config set compiler.libcxx=libstdc++11 linux-gcc54-amd64

## Contributing

If you want to contribute, please, create a fork or branch and submit a new MR.
