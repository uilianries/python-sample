#!/usr/bin/env python
# -*- coding: utf-8 -*-

import hello

def main():
    console = hello.console.Console()
    message = hello.console.Message('0xDEADBEEF')
    console.show(message)

if __name__ == "__main__":
    main()
