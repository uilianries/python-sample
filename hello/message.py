#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Message(object):
    """ Message wrapper
    """

    def __init__(self, data):
        self._data = data

    def __repr__(self):
        return "HELLO: {}".format(self._data)
