#!/usr/bin/env python
# -*- coding: utf-8 -*-

from termcolor import colored


class Console(object):
    """ Print a message on console
    """

    def show(self, message):
        """ Print colored message on console
        """
        text = colored(str(message), 'red', attrs=['reverse', 'blink'])
        print(text)
