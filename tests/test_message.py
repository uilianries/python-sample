#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest
from hello.message import Message

@pytest.fixture
def setup_message():
    import os
    return os.getenv('HELLO_MESSAGE')

def test_hello_message(setup_message):
    data = setup_message
    assert data is not None
    message = Message(data)
    assert data in str(message)
